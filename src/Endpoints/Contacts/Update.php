<?php
/**
 * This file is part of the cartel/hubspot library
 *
 * @package    cartel/hubspot
 * @author     Ryan Winchester <fungku@gmail.com>
 * @copyright  Copyright (c) Ryan Winchester
 * @license    http://mit-license.org/ MIT
 * @link       https://github.com/cartel/hubspot
 * Created:    2015-10-18  10:18 PM
 */

namespace SevenShores\Cartel\Hubspot\Endpoints\Contacts;

use SevenShores\Cartel\Endpoint;

class Update extends Endpoint
{
    protected $url = "https://api.hubapi.com/v1/contacts/{contact_id}";
    protected $method = "PUT";

    /**
     * @param int   $contactId
     * @param array $properties
     */
    public function __construct($contactId, $properties = [])
    {
        $this->urlParams['contact_id'] = $contactId;
        $this->options['json'] = $properties;
    }
}