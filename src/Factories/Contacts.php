<?php
/**
 * This file is part of the cartel/hubspot library
 *
 * @package    cartel/hubspot
 * @author     Ryan Winchester <fungku@gmail.com>
 * @copyright  Copyright (c) Ryan Winchester
 * @license    http://mit-license.org/ MIT
 * @link       https://github.com/cartel/hubspot
 * Created:    2015-10-18  10:18 PM
 */

namespace SevenShores\Cartel\Hubspot\Factories;

use SevenShores\Cartel\EndpointFactory;
use SevenShores\Cartel\Hubspot\Endpoints;

class Contacts extends EndpointFactory
{
    protected $endpoints = [
        'all'    => Endpoints\Contacts\All::class,
        'create' => Endpoints\Contacts\Create::class,
        'update' => Endpoints\Contacts\Update::class,
    ];
}
